package com.ielts.essay.be.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.ielts.essay.be.R
import com.ielts.essay.be.ui.fragment.WriteFragment
import kotlinx.android.synthetic.main.activity_test.*

class WriteActivity : AppCompatActivity() {
    private var writeFragment: WriteFragment? = null
    private var docId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        setSupportActionBar(toolbarPracticeTest)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        docId = intent.getIntExtra("docId", -1)
        writeFragment = WriteFragment.newInstance(docId)
        supportFragmentManager.beginTransaction().add(R.id.container, writeFragment!!).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
    }
}