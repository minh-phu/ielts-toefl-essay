package com.ielts.essay.be.ui.fragment

import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ielts.essay.be.R
import com.ielts.essay.be.model.Essay
import com.ielts.essay.be.utils.AdUtil
import kotlinx.android.synthetic.main.fragment_read.*


class ReadFragment : Fragment() {
    private var catId: Int = 0

    companion object {
        fun newInstance(catId: Int): ReadFragment {
            val contentFragment = ReadFragment()
            contentFragment.catId = catId
            return contentFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater.inflate(R.layout.fragment_read, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutNativeAdsTop = view.findViewById<CardView>(R.id.readAdsContainerTop)
        AdUtil.showNativeAdsFbSmallInList(context!!, layoutNativeAdsTop, R.string.adsFb_ReadEssayTop, R.string.adsGg_ReadEssayTop)

        val layoutNativeAdsBottom = view.findViewById<CardView>(R.id.readAdsContainerBottom)
        AdUtil.showNativeAdFbLarge(context!!, layoutNativeAdsBottom, R.string.adsFb_ReadEssayBottom, R.string.adsGg_ReadEssayBottom)

        val content = Essay.getById(catId)!!
        txtTitle.text = content.getTitleDesc()
        txtContent.text = fromHtml(content.getContentDesc())
    }

    private fun fromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }
}