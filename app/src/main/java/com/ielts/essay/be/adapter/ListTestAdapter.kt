package com.ielts.essay.be.adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ielts.essay.be.R
import com.ielts.essay.be.R.layout.item_category
import com.ielts.essay.be.model.Essay
import com.ielts.essay.be.utils.AdUtil
import com.ielts.essay.be.utils.Utils
import kotlinx.android.synthetic.main.item_category.view.*

class ListTestAdapter(private val context: Context, private var categoryList: List<Essay>) : RecyclerView.Adapter<ListTestAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(item_category, viewGroup, false))
    }

    override fun onBindViewHolder(itemViewHolder: ViewHolder, position: Int) {
        val category = categoryList[position]
        itemViewHolder.itemView.txtName.text = category.getTitleDesc()

        try {
            if ((position + 2) % Utils.ITEM_SHOW_ADS == 0) {
                val layoutNativeAds = itemViewHolder.itemView.findViewById<CardView>(R.id.adsContainer)
                if (layoutNativeAds.childCount == 0) {
                    AdUtil.showNativeAdsFbSmallInList(context, layoutNativeAds, R.string.adsFb_ListEssay, R.string.adsGg_ListEssay)
                }
            }
        } catch (e: Exception) {
        }
    }

    override fun getItemViewType(position: Int): Int = position

    override fun getItemCount(): Int = categoryList.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.btnItem.setOnClickListener {
                categoryEvent?.onItemCategoryClick(categoryList[adapterPosition].id.toInt())
            }
        }
    }


    var categoryEvent: ListCategoryView? = null

    interface ListCategoryView {
        fun onItemCategoryClick(docId: Int)
    }

}
