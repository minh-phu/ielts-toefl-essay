package com.ielts.essay.be.utils

object Constants {

    const val CHECK_PURCHASE = "hide_ad"

    const val CHECK_RATE = "is_rate"


    const val FILE_CORRECT = "correct"
    const val FILE_WRONG = "wrong"

    const val LESSON_NUMBER = "lesson_number"

    const val LIST_LESSON = "list_lesson"

    const val TEST_STYLE = "test_number"

    const val TEST_REMEMBER_VI = 0
    const val TEST_REMEMBER_EN = 1
    const val TYPE_REMEMBER = "type_remember"

    const val TEST_LISTEN_ONE = 2
    const val TEST_LISTEN_TWO = 3

}
