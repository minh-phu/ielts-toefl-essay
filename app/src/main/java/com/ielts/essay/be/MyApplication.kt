package com.ielts.essay.be

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.google.android.gms.ads.MobileAds
import com.ielts.essay.be.utils.CipherHelper
import com.ielts.essay.be.utils.Utils
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration

class MyApplication : Application() {

    companion object {
        lateinit var sharedPreferences: SharedPreferences
    }

    override fun onCreate() {
        super.onCreate()

        MobileAds.initialize(applicationContext, getString(R.string.app_ads_id))
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val key = sharedPreferences.getString("app_id", "")
        if (key!!.isEmpty() || key.length != 50) {
            val editor = sharedPreferences.edit()
            editor.putString("app_id", getString(R.string.app_id))
            editor.apply()
        }

        Realm.init(this)
        val config = RealmConfiguration.Builder().assetFile("assets.zip").build()
        Realm.setDefaultConfiguration(config)

        Utils.initKey(this)
        CipherHelper.init(resources.getString(R.string.dev_url))

        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())

    }
}
