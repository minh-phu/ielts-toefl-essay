package com.ielts.essay.be.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.ielts.essay.be.R
import com.ielts.essay.be.ui.fragment.ReadFragment
import kotlinx.android.synthetic.main.activity_test.*

class ReadActivity : AppCompatActivity() {
    private var testFragment: ReadFragment? = null
    private var docId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        setSupportActionBar(toolbarPracticeTest)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        docId = intent.getIntExtra("docId", 0)
        testFragment = ReadFragment.newInstance(docId)
        supportFragmentManager.beginTransaction().add(R.id.container, testFragment!!).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (docId > 424)
            menuInflater.inflate(R.menu.menu_write, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.write_essay -> openWriteScreen()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openWriteScreen(){
        val intent = Intent(this, WriteActivity::class.java)
        intent.putExtra("docId", docId)
        startActivityForResult(intent, MainActivity.RESULT_SHOW_ADS)
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
    }
}