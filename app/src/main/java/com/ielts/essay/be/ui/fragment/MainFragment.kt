package com.ielts.essay.be.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.ielts.essay.be.R
import com.ielts.essay.be.adapter.ListTestAdapter
import com.ielts.essay.be.model.Essay
import com.ielts.essay.be.ui.activity.MainActivity.Companion.COUNT_STORY_ADS
import com.ielts.essay.be.ui.activity.MainActivity.Companion.IELTS
import com.ielts.essay.be.ui.activity.MainActivity.Companion.MY_ESSAY
import com.ielts.essay.be.ui.activity.MainActivity.Companion.RESULT_SHOW_ADS
import com.ielts.essay.be.ui.activity.ReadActivity
import com.ielts.essay.be.utils.purchase.CheckPurchase
import kotlinx.android.synthetic.main.fragment_category.*

class MainFragment : Fragment(), ListTestAdapter.ListCategoryView {
    private var subject: String = IELTS
    private var cat: List<Essay>? = null
    private var categoryAdapter: ListTestAdapter? = null

    companion object {
        fun newInstance(subject: String): MainFragment {
            val categoryFragment = MainFragment()
            categoryFragment.subject = subject
            return categoryFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.fragment_category, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPopupFb()

        cat = Essay.getAllByType(subject.toLowerCase())
        categoryAdapter = ListTestAdapter(activity!!, cat!!)
        categoryAdapter?.categoryEvent = this
        recycler_view.layoutManager = LinearLayoutManager(activity)
        recycler_view.adapter = categoryAdapter
    }

    override fun onItemCategoryClick(docId: Int) {
        val intent = Intent(activity, ReadActivity::class.java)
        intent.putExtra("docId", docId)
        startActivityForResult(intent, RESULT_SHOW_ADS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (subject == MY_ESSAY){
            (cat as ArrayList).clear()
            (cat as ArrayList<Essay>).addAll(Essay.getAllByType(subject.toLowerCase()))
            categoryAdapter?.notifyDataSetChanged()
        }

        if (requestCode == RESULT_SHOW_ADS) {
            showAds++
            if (showAds >= COUNT_STORY_ADS) {
                if (!CheckPurchase.checkPurchaseAd(activity)) {
                    try {
                        showInterstitialAds()
                    } catch (e: Exception) {

                    }
                }
            }
        }
    }


    private var mInterstitialAdGg: InterstitialAd? = null
    private var mInterstitialAdFb: com.facebook.ads.InterstitialAd? = null
    private var showAds = 0

    private fun initPopupAdGg() {
        if (activity != null) {
            mInterstitialAdGg = InterstitialAd(activity)
            mInterstitialAdGg?.adUnitId = getString(R.string.adsGg_Popup)
            if (!mInterstitialAdGg!!.isLoading && !mInterstitialAdGg!!.isLoaded) {
                val adRequest = AdRequest.Builder().build()
                mInterstitialAdGg?.loadAd(adRequest)
            }
            mInterstitialAdGg?.adListener = object : AdListener() {
                override fun onAdClosed() {
                    if (!mInterstitialAdGg!!.isLoading && !mInterstitialAdGg!!.isLoaded) {
                        val adRequest = AdRequest.Builder().build()
                        mInterstitialAdGg?.loadAd(adRequest)
                    }
                }
            }
        }
    }

    private fun initPopupFb() {
        if (activity != null) {
            mInterstitialAdFb = com.facebook.ads.InterstitialAd(activity, getString(R.string.adsFb_Popup))
            mInterstitialAdFb?.setAdListener(object : com.facebook.ads.InterstitialAdListener {
                override fun onAdClicked(p0: Ad?) {}

                override fun onAdLoaded(p0: Ad?) {}

                override fun onLoggingImpression(p0: Ad?) {}

                override fun onInterstitialDisplayed(ad: Ad) {}

                override fun onInterstitialDismissed(ad: Ad) {
                    mInterstitialAdFb!!.loadAd()
                }

                override fun onError(ad: Ad, adError: AdError) {
                    initPopupAdGg()
                }
            })
            mInterstitialAdFb?.loadAd()
        }
    }


    private fun showInterstitialAds() {
        if (mInterstitialAdFb != null && mInterstitialAdFb!!.isAdLoaded) {
            mInterstitialAdFb?.show()
            showAds = 0
        } else {
            if (mInterstitialAdGg != null && mInterstitialAdGg!!.isLoaded) {
                mInterstitialAdGg!!.show()
                showAds = 0
            }
        }
    }


    override fun onDetach() {
        mInterstitialAdGg = null
        mInterstitialAdFb?.destroy()
        super.onDetach()
    }

    override fun onDestroy() {
        mInterstitialAdGg = null
        mInterstitialAdFb?.destroy()
        super.onDestroy()
    }
}

