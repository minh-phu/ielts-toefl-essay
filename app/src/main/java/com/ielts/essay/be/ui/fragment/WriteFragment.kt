package com.ielts.essay.be.ui.fragment


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ielts.essay.be.R
import com.ielts.essay.be.model.Essay
import com.ielts.essay.be.utils.AdUtil
import com.vicpin.krealmextensions.delete
import com.vicpin.krealmextensions.save
import kotlinx.android.synthetic.main.fragment_write.*
import kotlinx.android.synthetic.main.fragment_write.view.*

/**
 * Created by phutruonghao on 7/22/17.
 */

class WriteFragment : Fragment() {

    private var docId = -1
    private var myEssay: Essay? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? {
        val view = inflater.inflate(R.layout.fragment_write, container, false)

        if (docId != -1){
            myEssay = Essay.getById(docId)
            view.txtTitle.setText(myEssay!!.getTitleDesc())
            view.txtContent.setText(myEssay!!.getContentDesc())
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutNativeAds = view.findViewById<CardView>(R.id.writeAdsContainer)
        AdUtil.showNativeAdFbLarge(context!!, layoutNativeAds, R.string.adsFb_WriteEssay, R.string.adsGg_ReadEssayBottom)

        btnRandom.setOnClickListener {
            txtTitle.setText(Essay.getRandomTitle())
        }

        btnDelete.setOnClickListener {
            if (docId != -1) {
                myEssay!!.delete { equalTo("id", myEssay!!.id) }
            }

            activity!!.finish()
        }

        btnSave.setOnClickListener {
            if (txtTitle.text == null) {

            }

            if (docId == -1) {
                myEssay = Essay()
                myEssay!!.id = Essay.getLastIndex().toLong() + 1
                myEssay!!.type = "my"
            }

            myEssay!!.title = txtTitle.text.toString()
            myEssay!!.content = txtContent.text.toString()
            myEssay!!.save()

            val returnIntent = Intent()
            activity!!.setResult(Activity.RESULT_OK, returnIntent)
            activity!!.finish()
        }
    }

    companion object {

        fun newInstance(docId: Int): WriteFragment {
            val writeFragment = WriteFragment()
            writeFragment.docId = docId
            return writeFragment
        }
    }
}
