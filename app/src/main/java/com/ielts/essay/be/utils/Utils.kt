package com.ielts.essay.be.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.text.Html
import android.text.Spanned
import android.view.View
import android.widget.TextView
import com.ielts.essay.be.MyApplication
import com.ielts.essay.be.R
import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException



object Utils {

    lateinit var key: String
    const val PASSED_SCORE = 7
    const val ITEM_SHOW_ADS = 8

    fun isNetworkConnected(context: Context): Boolean {
        val cm: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo != null
    }

    @Suppress("DEPRECATION")
    fun fromHtml(html: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

    fun initKey(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val k = sharedPreferences.getString("app_id", "")!! + "lWW/4T+x0ZW7dw"
        key = ""
        try {
            key = AESCrypt.decrypt("e270c533a65a8d863f12301a568b8e47", k)
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }

    }

    fun checkUserRateApp(): Boolean {
        return MyApplication.sharedPreferences.getBoolean("IsRate", false)
    }

    fun showDialogRate(activity: Activity) {
        val dialogBuilder = android.support.v7.app.AlertDialog.Builder(activity)
        val dialogView = View.inflate(activity, R.layout.dialog_rating_app, null)
        dialogBuilder.setView(dialogView)
        val alertDialogRating = dialogBuilder.create()

        dialogView.findViewById<TextView>(R.id.btnRatingApp).setOnClickListener {
            val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=${activity.packageName}"))
            marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            activity.startActivity(marketIntent)
            val editor = MyApplication.sharedPreferences.edit()
            editor.putBoolean("IsRate", true)
            editor.apply()
            alertDialogRating?.dismiss()
        }
        dialogView.findViewById<TextView>(R.id.btnCancel).setOnClickListener {
            activity.finish()
        }

        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 80)
        alertDialogRating?.setCanceledOnTouchOutside(false)
        alertDialogRating?.window?.setBackgroundDrawable(inset)
        // alertDialogRating?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        alertDialogRating?.show()
    }

}
