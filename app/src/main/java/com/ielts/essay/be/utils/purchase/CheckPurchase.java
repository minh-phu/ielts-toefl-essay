package com.ielts.essay.be.utils.purchase;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class CheckPurchase {

    public static boolean checkPurchaseAd(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean("hide_ad", false);
    }


}
