package com.ielts.essay.be.model

import com.ielts.essay.be.utils.CipherHelper
import com.vicpin.krealmextensions.count
import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Essay : RealmObject() {

    @PrimaryKey
    var id: Long = 0
    var title: String? = null
    var content: String? = null
    var type: String? = null

    fun getTitleDesc(): String {
        return if (type == "my")
            title!!
        else
            CipherHelper.decrypt(title!!)
    }

    fun getContentDesc(): String {
        return if (type == "my")
            content!!
        else
            CipherHelper.decrypt(content!!)
    }

    companion object {

        fun getAllByType(type: String): List<Essay> {
            return Essay().query { equalTo("type", type) }
        }

        fun getLastIndex(): Int {
            return Essay().count().toInt()
        }

        fun getById(cat_id: Int): Essay? {
            return Essay().queryFirst { equalTo("id", cat_id) }
        }

        fun getRandomTitle(): String {
            val r = Random().nextInt(Essay().count().toInt())
            return Essay().queryFirst { equalTo("id", r) }!!.getTitleDesc()
        }

    }

}