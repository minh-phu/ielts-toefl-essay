package com.ielts.essay.be.ui.activity

import android.app.AlertDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import com.android.vending.billing.IInAppBillingService
import com.google.android.gms.ads.MobileAds
import com.ielts.essay.be.R
import com.ielts.essay.be.R.id.btnChangeCategory
import com.ielts.essay.be.R.id.txtLevel
import com.ielts.essay.be.ui.fragment.MainFragment
import com.ielts.essay.be.ui.promoteapp.AppPromote
import com.ielts.essay.be.ui.promoteapp.AppPromoteAdapter
import com.ielts.essay.be.utils.Constants
import com.ielts.essay.be.utils.Utils
import com.ielts.essay.be.utils.purchase.CheckPurchase
import com.ielts.essay.be.utils.purchase.IabHelper
import com.ielts.essay.be.utils.purchase.IabResult
import com.ielts.essay.be.utils.purchase.Purchase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.bottom_sheet_category.view.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
        IabHelper.OnIabPurchaseFinishedListener, IabHelper.OnIabSetupFinishedListener,
        AppPromoteAdapter.OnPromoteClick {

    companion object {
        const val RESULT_SHOW_ADS = 1000

        const val COUNT_STORY_ADS = 2

        const val IELTS = "IELTS"
        const val TOEFL = "TOEFL"
        const val MY_ESSAY = "MY"
    }

    var currentEssay: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        MobileAds.initialize(this, getString(R.string.app_ads_id))
        restorePurchase()


        setUpDrawer()
        loadNewCategory(IELTS, IELTS)

        btnChangeCategory.setOnClickListener {
            val bottomSheetDialog = BottomSheetDialog(this)
            val sheetView = layoutInflater.inflate(R.layout.bottom_sheet_category, null)

            sheetView.btnBgn.setOnClickListener {
                currentEssay = 1
                loadNewCategory(IELTS, sheetView.btnBgn.text.toString())
                bottomSheetDialog.dismiss()
            }

            sheetView.btnInter.setOnClickListener {
                currentEssay = 2
                loadNewCategory(TOEFL, sheetView.btnInter.text.toString())
                bottomSheetDialog.dismiss()
            }

            sheetView.btnAdv.setOnClickListener {
                currentEssay = 3
                loadNewCategory(MY_ESSAY, sheetView.btnAdv.text.toString())
                bottomSheetDialog.dismiss()
            }

            bottomSheetDialog.setContentView(sheetView)
            bottomSheetDialog.show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_write, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.write_essay -> openWriteScreen()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openWriteScreen() {
        val intent = Intent(this, WriteActivity::class.java)
        startActivityForResult(intent, RESULT_SHOW_ADS)
    }


    private fun loadNewCategory(level: String, text: String) {
        txtLevel.text = text
        val transaction = supportFragmentManager.beginTransaction()
        val categoryFragment = MainFragment.newInstance(level)
        transaction.replace(R.id.mainContainer, categoryFragment)
        transaction.commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_data_policy -> {
                if (Utils.isNetworkConnected(this)) {
                    val devIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.policy_page)))
                    devIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(devIntent)
                }
            }
            R.id.action_remove_ads -> {
                if (Utils.isNetworkConnected(this)) {
                    startPurchase()
                }
            }
            R.id.action_share_app -> {
                val shareBody = "https://play.google.com/store/apps/details?id=$packageName"
                val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name))
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.app_name)))
            }
            R.id.action_rate -> openRate()
        }
        return true
    }

    private fun setUpDrawer() {
        val toggle = ActionBarDrawerToggle(
                this,
                drawerMain,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        )
        drawerMain.addDrawerListener(toggle)
        toggle.syncState()

        val v = navigationMain.inflateHeaderView(R.layout.more_app)
        val rcvMoreApp = v.findViewById<RecyclerView>(R.id.rcvMoreApp)
        val adapter = AppPromoteAdapter(
                AppPromote.getListPromoteApp(this@MainActivity), this
        )
        rcvMoreApp.layoutManager = LinearLayoutManager(this@MainActivity)
        rcvMoreApp.adapter = adapter
        navigationMain.setNavigationItemSelectedListener(this)
    }

    override fun onItemPromoteClick(item: AppPromote) {
        if (Utils.isNetworkConnected(this@MainActivity)) {
            when {
                item.isMoreApp -> {
                    val devIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.dev_page)))
                    devIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(devIntent)
                }
                else -> openPromo(item.appUrl)
            }
        } else {
            Toast.makeText(
                    this@MainActivity, getString(R.string.txt_check_internet), Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun openPromo(url: String) {
        try {
            val marketIntent = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$url"))
            marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(marketIntent)
        } catch (e: Exception) {
            startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$url")
                    )
            )
        }
    }

    private fun openRate() {
        if (Utils.isNetworkConnected(this)) {
            try {
                val marketIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName"))
                marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_MULTIPLE_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(marketIntent)
            } catch (e: Exception) {
                startActivity(
                        Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                        )
                )
            }
        } else {
            Toast.makeText(
                    this@MainActivity,
                    getString(R.string.txt_check_internet),
                    Toast.LENGTH_SHORT
            ).show()
        }
    }


    override fun onBackPressed() {
        if (drawerMain.isDrawerOpen(GravityCompat.START)) {
            drawerMain.closeDrawer(GravityCompat.START)
        } else if (!Utils.checkUserRateApp()) {
            Utils.showDialogRate(this)
        } else {
            super.onBackPressed()
        }
    }


    public override fun onDestroy() {
        if (mService != null) {
            unbindService(mServiceConn)
        }
        super.onDestroy()
    }


    private var mHelper: IabHelper? = null
    var mService: IInAppBillingService? = null
    private val skuRemoveAds = "com.ielts.essay.be.removeads"
    private var base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA08bf9fAoLBoEGO+287WZkN3Tle7R+cYs8tpSwfZM+AO/ldvvs6i8nWFD+eoDAnFCp4r3wKQQg2ooAs9nKg+EdUt9UJf0oRF3BeObRIFvT6K8hqFpJrOwx8TNk1IWISjyTd1hVV+x0EJIe1GXZQqW6LQs1YV9xu6p79++/jSx4Lp99Qc3z7qK072bAFY3qCO27Oh9Ihp4KrtKXSwAyM9vwO28t+DTmzEtljuossCsvcbpfF3u9peAUnu17Dx6Zps//9NV3dk3X7lKeV+4oN9JGii15QrqgzvI2zyTEMDlT7IT5CJUF+nvgqZaRc2DKoQ8Xk17PdJHYGWRtQQ04SHm0QIDAQAB"

    private fun restorePurchase() {
        if (!CheckPurchase.checkPurchaseAd(this@MainActivity)) {
            navigationMain.menu.findItem(R.id.action_remove_ads).isVisible = true
            val serviceIntent = Intent("com.android.vending.billing.InAppBillingService.BIND")
            serviceIntent.`package` = "com.android.vending"
            bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE)
        }
    }

    private var mServiceConn: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mService = IInAppBillingService.Stub.asInterface(service)
            val ownedItems: Bundle
            try {
                ownedItems = mService!!.getPurchases(3, packageName, "inapp", null)
                val response = ownedItems.getInt("RESPONSE_CODE")
                if (response != 0) {
                    return
                }

                val ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST")
                if (ownedSkus != null) {
                    (0 until ownedSkus.size)
                            .filter { ownedSkus[it] == skuRemoveAds }
                            .forEach { successPurchaseRemoveAds() }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    fun successPurchaseRemoveAds() {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this@MainActivity)
        val editor = sharedPreferences.edit()
        editor.putBoolean(Constants.CHECK_PURCHASE, true).apply()
        val builder1 = AlertDialog.Builder(this@MainActivity)
        builder1.setTitle(getString(R.string.title_remove_ads))
        builder1.setMessage(getString(R.string.mess_remove_ads))
        builder1.setCancelable(false)
        builder1.setPositiveButton(getString(R.string.ok)) { _, _ -> finish() }
        val alert11 = builder1.create()
        alert11.show()
    }

    private fun startPurchase() {
        mHelper = IabHelper(this@MainActivity, base64EncodedPublicKey)
        mHelper?.startSetup(this@MainActivity)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(currentEssay==3){
            loadNewCategory(MY_ESSAY, "MY ESSAY")
        }
        if (mHelper != null) {
            if (!mHelper!!.handleActivityResult(requestCode, resultCode, data)) {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onIabSetupFinished(result: IabResult) {
        if (result.isSuccess) {
            try {
                mHelper?.launchPurchaseFlow(this@MainActivity, skuRemoveAds, 10001, this@MainActivity)
            } catch (e: IabHelper.IabAsyncInProgressException) {
                e.printStackTrace()
            }
        }
    }

    override fun onIabPurchaseFinished(result: IabResult, purchase: Purchase?) {
        if (mHelper == null) {
            return
        }
        val responseCode: Int = result.response
        if (result.isFailure && responseCode != 7) {
            return
        }
        if (responseCode == 7 || purchase!!.sku == skuRemoveAds) {
            successPurchaseRemoveAds()
        }

    }

}
